from django.db import models
from django.core.validators import MinValueValidator
# Create your models here.
class COVIDStat(models.Model):
    date_accessed = models.DateTimeField()
    confirmed = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    deaths = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    recovered = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    active = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    
    # util method to check if no change has occurred
    # since last API access, instead of overriding
    # __eq__ which might mess up the database
    def change(other_entry, self):
        return {
           'confirmed': self.confirmed - other_entry.confirmed,
           'deaths': self.deaths - other_entry.deaths,
           'recovered': self.recovered - other_entry.recovered,
           'active': self.active - other_entry.active
           }