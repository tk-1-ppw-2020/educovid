from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from datetime import datetime, timedelta, date, time
from django.forms.models import model_to_dict
from django.utils.timezone import make_aware
from .models import COVIDStat
from register.forms import LoginForm
import requests
import json

from numpy import polyfit, poly1d
# Create your views here.

def index(request):
    context = json.loads(access_db_and_return_latest(request).content)
    uname: str = request.user.username
    context["greeting"] = "Hello, " + (uname if len(uname) <= 10 else (uname[0:5] + "..."))
    context["login_form"] = LoginForm
    return render(request, 'covid_count/index.html', context)

def table(request):
    uname: str = request.user.username
    context = {'database' : COVIDStat.objects.all(),
    "greeting": "Hello, " + (uname if len(uname) <= 10 else (uname[0:5] + "...")),
    "login_form": LoginForm}
    return render(request, 'covid_count/table.html', context)

def get_db(request):
    url = "https://api.kawalcorona.com/indonesia/"
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    json_list = json.loads(response.text.encode('utf8'))
    return_dict = json_list[-1]
    return JsonResponse(return_dict)

def force_add_db(request):
    dict_to_check = request.POST.dict()
    value_validator = set('0123456789,')
    key_validator = set({'positif', 'meninggal', 'sembuh', 'dirawat'})
    try:
        if (set(dict_to_check.keys()) == key_validator
        and (set("".join(dict_to_check.values())).issubset(value_validator))):
            add_db(request.POST)
            return HttpResponse(status=204)
        else:
            return HttpResponseBadRequest()
    except:
        return HttpResponseBadRequest()

def access_db_and_return_latest(request):
    access_date = make_aware(datetime.now().replace(microsecond=0))
    dict_to_check = json.loads(get_db(request).content)
    temp = COVIDStat(date_accessed = access_date,
                     confirmed     = int(dict_to_check['positif'].replace(",", "")),
                     deaths        = int(dict_to_check['meninggal'].replace(",", "")),
                     recovered     = int(dict_to_check['sembuh'].replace(",", "")),
                     active        = int(dict_to_check['dirawat'].replace(",", ""))
                     )
    access_db(temp)
    return JsonResponse(model_to_dict(COVIDStat.objects.latest('date_accessed')))
    
def add_db(dict_to_check):
    access_date = make_aware(datetime.now().replace(microsecond=0))
    temp = COVIDStat(date_accessed = access_date,
                     confirmed     = int(dict_to_check['positif'].replace(",", "")),
                     deaths        = int(dict_to_check['meninggal'].replace(",", "")),
                     recovered     = int(dict_to_check['sembuh'].replace(",", "")),
                     active        = int(dict_to_check['dirawat'].replace(",", ""))
                     )
    access_db(temp)
    
def access_db(temp):
    try:
        latest_change = tuple(temp.change(COVIDStat.objects.latest('date_accessed')).values())
        if latest_change != (0, 0, 0, 0):
            temp.save()
        else:
            del temp
    except COVIDStat.DoesNotExist:
        temp.save()

def predict(request):
    return_json = {}
    # extract all stats into lists
    date_accessed_list = []
    confirmed_list     = []
    deaths_list        = []
    recovered_list     = []
    active_list        = []
    if COVIDStat.objects.count() == 0:
        return JsonResponse(return_json)
    all_stats = COVIDStat.objects.all()
    for stat in all_stats:
        date_accessed_list.append(int(stat.date_accessed.timestamp()))
        confirmed_list.append(stat.confirmed)
        deaths_list.append(stat.deaths)
        recovered_list.append(stat.recovered)
        active_list.append(stat.active)
    
    tomorrow = int((make_aware(datetime.now().replace(microsecond=0)) + timedelta(days = 1)).timestamp())
    return_json['positif'] = int(poly1d(polyfit(date_accessed_list, confirmed_list, 1))(tomorrow))
    return_json['meninggal'] = int(poly1d(polyfit(date_accessed_list, deaths_list, 1))(tomorrow))
    return_json['sembuh'] = int(poly1d(polyfit(date_accessed_list, recovered_list, 1))(tomorrow))
    return_json['dirawat'] = int(poly1d(polyfit(date_accessed_list, active_list, 1))(tomorrow))
    return JsonResponse(return_json)

