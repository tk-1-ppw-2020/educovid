from django.urls import path
from . import views

app_name = 'covid_count'

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.access_db_and_return_latest, name='add'),
    path('table/', views.table, name='table'),
    path('get_db/', views.get_db, name='get_db'),
    path('force_add_db', views.force_add_db, name='force_add_db'),
    path('predict', views.predict, name='predict')
]