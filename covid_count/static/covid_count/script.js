var tooltip_flag = 0;
var current_result = null;
$(document).ready(function () {
    
    $( '#force-get-API' ).click(function ( event ) {
        event.preventDefault();
        $.ajax({
            url: window.location.origin + "/get_db",
            crossDomain: true,
            success: displayResultGET
        });
    });
    
    $( '#predict' ).click(function ( event ) {
        event.preventDefault();
        $.ajax({
            url: window.location.origin + "/predict",
            crossDomain: true,
            success: displayPredictions
        });
    });
    
    $( '#force-add-data' ).click(function ( event ) {
        event.preventDefault();
        if (current_result != null) {
            const csrftoken = getCookie('csrftoken');
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: current_result,
                headers : {
                    'X-CSRFToken': csrftoken
                },
                url: window.location.origin + '/force_add_db',
                success: function ( result ) {
                    current_result = null;
                    console.log('Success!');
                }
            });
        }
        else {
            // do nothing
        }
    });
    
    $( '#prediction-tooltip' ).mouseleave( function ( event ) {
        event.preventDefault();
        if (tooltip_flag == 1) {
            $( '#prediction-tooltip' ).prop('title', 'This prediction works by using advanced machine learning algorithms and taking into account various variables outside of just the current data such as weather conditions and stock options fluctuations');
            tooltip_flag = 0;
        } else {
            $( '#prediction-tooltip' ).prop('title', "Just kidding it just uses numpy's polyfit");
            tooltip_flag = 1;
        }
    });
        

});

function displayPredictions ( result ) {
    $( '#predict-table' ).css( "display", "flex" );
    $( '#predict-table-tally-positif' ).html( result.positif );
    $( '#predict-table-tally-meninggal' ).html( result.meninggal );
    $( '#predict-table-tally-sembuh' ).html( result.sembuh );
    $( '#predict-table-tally-aktif' ).html( result.dirawat );
}


function displayResultGET ( result ) {
    current_result = result;
    $( '#force-get-API-table' ).css( "display", "flex" );
    $( '#force-get-API-table-tally-positif' ).html( parseInt(result.positif.replace(",", "")) );
    $( '#force-get-API-table-tally-meninggal' ).html( parseInt(result.meninggal.replace(",", "")) );
    $( '#force-get-API-table-tally-sembuh' ).html( parseInt(result.sembuh.replace(",", "")) );
    $( '#force-get-API-table-tally-aktif' ).html( parseInt(result.dirawat.replace(",", "")) );
}

// courtesy of https://docs.djangoproject.com/en/3.1/ref/csrf/#ajax
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
const csrftoken = getCookie('csrftoken');