from django.test import TestCase, Client
from django.apps import apps
from .models import RumahSakit
from .apps import RsRujukanConfig
from django.contrib.auth.models import User
from register.models import Register
from django.contrib.auth import authenticate, login


# Create your tests here.
class rs_rujukanTest(TestCase):
	#app	
	def test_apakah_aplikasi_ada(self):
		self.assertEqual(RsRujukanConfig.name, 'rs_rujukan')
		self.assertEqual(apps.get_app_config('rs_rujukan').name, 'rs_rujukan')
	#url
	def test_apakah_url_rs_rujukan_ada(self):
		response = Client().get('/rs_rujukan/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_url_tambah_rs_ada(self):
		user = User.objects.create_superuser(username="fayeeed", password="teskatasandi123")
		client = Client()
		responsePost = client.post('/register/logon/', {'username': 'fayeeed', 'password':'teskatasandi123'} )
		response = client.get('/rs_rujukan/add_rs/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_url_tambah_rs_melakukan_redirect_jika_bukan_admin(self):
		user = User.objects.create_user(username="fayeeed", password="teskatasandi123")
		client = Client()
		responsePost = client.post('/register/logon/', {'username': 'fayeeed', 'password':'teskatasandi123'} )
		response = client.get('/rs_rujukan/add_rs/')
		self.assertEquals(response.status_code, 302)

	#template
	def test_apakah_halaman_rs_rujukan_memiliki_template(self):
		response =  Client().get('/rs_rujukan/')
		self.assertTemplateUsed(response, 'rs_rujukan/rs_rujukan.html')

	#model
	def test_apakah_objek_RumahSakit_apabila_dicetak_akan_mereturn_namanya(self):
		objek = RumahSakit.objects.create(nama="nama", provinsi="provinsi", 
			telepon="telepon", alamat="alamat")
		self.assertIn(str(objek), "nama")

	#not-auth
	def test_apakah_tanpa_auth_halaman_tidak_akan_menampilkan_fitur_cari_rs_terdekat(self):
		responseGet = Client().get('/rs_rujukan/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("Harap login untuk menggunakan fitur ini", html_response)

	#auth
	def test_apakah_auth_sudah_memiliki_fitur_cari_rs_terdekat(self):
		user = User.objects.create_user(username="fayeeed", password="teskatasandi123")
		Register.objects.create(user=user, province="ACEH")
		client = Client()
		responsePost = client.post('/register/logon/', {'username': 'fayeeed', 'password':'teskatasandi123'} )
		responseGet = client.get('/rs_rujukan/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("Hello, fayeeed", html_response)
		self.assertIn("RS Rujukan Provinsi Terdekat", html_response)

	#data
	def test_apakah_url_data_ada(self):
		response =  Client().get('/rs_rujukan/rs_api/')
		self.assertEquals(response.status_code, 200)

	#add_data
	def test_apakah_form_berhasil_menambah_data(self):
		user = User.objects.create_superuser(username="fayeeed", password="teskatasandi123")
		Register.objects.create(user=user, province="ACEH")
		client = Client()
		client.post('/register/logon/', {'username': 'fayeeed', 'password':'teskatasandi123'} )
		responsePost = client.post('/rs_rujukan/add_rs/', {'nama': 'RStest', 'alamat':'alamattest', 'telepon':'telepontest', 'provinsi':'provinsitest'} )
		html_response = responsePost.content.decode('utf8')
		self.assertIn("Berhasil", html_response)
		responseGet = client.get('/rs_rujukan/rs_api/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("RStest", html_response)


