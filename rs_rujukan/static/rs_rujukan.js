$(document).ready(function() {
	$('#link').hide();
	$("#call").click(function(){
		$('#link')[0].click();
	})

	var tabel = $('#mytable').DataTable({
		"order": [[ 1, "asc" ]],
		ajax: {
			url : "/rs_rujukan/rs_api",
			dataSrc: "",
		},
		columns: [
			{ data: 'fields.nama'},
			{ data: 'fields.provinsi'},
			{ data: 'fields.alamat', className:"alamat" },
			{ data: 'fields.telepon', className:"telepon" },
		],

		"language": {
	      "emptyTable": "🔃 Mengambil Data Rumah Sakit...",
	      "search" : "Cari Rumah Sakit : ",
	    },

	    responsive: true,
	});


	new $.fn.dataTable.FixedHeader( tabel );
})