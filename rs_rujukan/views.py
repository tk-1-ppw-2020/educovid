from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Q
from .models import RumahSakit
from register.forms import LoginForm
from django.contrib.auth.models import User
from register.models import Register
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate, login
from django.core.serializers import serialize
from django.contrib.auth.decorators import login_required
from django.contrib import messages
import requests
import json

# Create your views here.
def index(request):
	response = {}
	data = RumahSakit.objects.all()
	response = {'data':data}
	
	# Navbar Login Section
	uname: str = request.user.username	
	response["greeting"] = "Hello, " + (uname if len(uname) <= 10 else (uname[0:5] + "..."))
	response["login_form"] = LoginForm

	if (request.user.is_authenticated):
		user = get_object_or_404(User, username=request.user.username)
		profile = get_object_or_404(Register, user=user)
		response['provinsi'] = profile.province

	return render(request, 'rs_rujukan/rs_rujukan.html', response)

def rs_api(request):
	data = RumahSakit.objects.all()
	data_list = serialize('json', data)
	return HttpResponse(data_list, content_type="text/json-comment-filtered")\

def add_rs(request):
	if request.user.is_superuser :
		response = {}
		if (request.method == 'POST'):
			nama = request.POST.get('nama')
			provinsi = request.POST.get('provinsi')
			alamat = request.POST.get('alamat')
			telepon = request.POST.get('telepon')
			RumahSakit.objects.create(nama=nama, provinsi=provinsi, telepon=telepon, alamat=alamat)

		return render(request, 'rs_rujukan/add_rs.html', response)

	messages.error(request, "❌ Akses Ditolak ❌")
	return redirect('/rs_rujukan')

# def data_rs(request):
# 	url = "https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/RS_Rujukan_Update_May_2020/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
# 	response = requests.get(url)
# 	data = response.json()
# 	return JsonResponse(data, safe = False)
