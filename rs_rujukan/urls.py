from django.contrib import admin
from django.urls import path
from rs_rujukan import views

app_name = "rs_rujukan"

urlpatterns = [
   	path('', views.index, name='index'),
   	# path('data_rs/', views.data_rs, name='data_rs'),
   	path('rs_api/', views.rs_api, name='rs_api'),
   	path('add_rs/', views.add_rs, name='add_rs'),
]