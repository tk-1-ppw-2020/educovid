from django.db import models


class Gejala(models.Model):
    demam = models.BooleanField(default=False)
    batuk = models.BooleanField(default=False)
    lelah = models.BooleanField(default=False)
    diare = models.BooleanField(default=False)
    sakit_tenggorokan = models.BooleanField(default=False)
    sakit_kepala = models.BooleanField(default=False)
    ruam = models.BooleanField(default=False)
    rasa_bau = models.BooleanField(default=False)
    sakit_dada = models.BooleanField(default=False)
    kesusahan_bernapas = models.BooleanField(default=False)
    score = models.SmallIntegerField(default=0, editable=False)

    def save(self, *args, **kwargs):
        score = 0
        if self.demam:
            score += 10
        if self.batuk:
            score += 10
        if self.lelah:
            score += 10
        if self.diare:
            score += 10
        if self.sakit_tenggorokan:
            score += 10
        if self.sakit_kepala:
            score += 10
        if self.ruam:
            score += 10
        if self.rasa_bau:
            score += 10
        if self.sakit_dada:
            score += 60
        if self.kesusahan_bernapas:
            score += 60
        self.score = score
        
        return super(Gejala, self).save(*args, **kwargs)
        