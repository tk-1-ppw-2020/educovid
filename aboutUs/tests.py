from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from django.http import HttpRequest
from aboutUs.models import KritikSaran
from django.urls import resolve, reverse
from . import views
from django.apps import apps


class AboutUsUnitTest(TestCase):
    def test_aboutUs_url_is_exist(self):
        response=Client().get('/aboutUs/')
        self.assertEqual(response.status_code,200)

    def test_aboutUs_template_used(self):
        response = Client().get('/aboutUs/')
        self.assertTemplateUsed(response, 'aboutUs/aboutUs.html')

    def test_aboutUs_funcView(self):
        found = resolve('/aboutUs/')
        self.assertEquals(found.func, views.AboutUs)

    def test_get_aboutUs_func(self):
        response = Client().get('/aboutUs/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'aboutUs/aboutUs.html')

    def test_get_aboutUs_kegiatan(self):
        KritikSaran.objects.create(kritik_saran='Test Kritik Saran')
        kritik_saran = KritikSaran.objects.get(kritik_saran='Test Kritik Saran')
        self.assertEqual(str(kritik_saran), 'Test Kritik Saran')

    def test_semangat_form_invalid(self):
        Client().post('/aboutUs/aboutUs/showText', data={})
        jumlah = KritikSaran.objects.filter(kritik_saran='Webnya sangat mengedukasi!').count()
        self.assertEqual(jumlah, 0)

    def test_model_kritiksaran( self):
        KritikSaran.objects.create(kritik_saran='Semangat!')
        ks = KritikSaran.objects.get(kritik_saran='Semangat!')
        self.assertEqual(str(ks), 'Semangat!')

    def test_get_aboutUs_func_showText(self):
        response = Client().get('/aboutUs/aboutUs/tampilkan-kritik/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'aboutUs/showText.html')





    


