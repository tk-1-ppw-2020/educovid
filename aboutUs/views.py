from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import KritikSaranForm
from .models import KritikSaran
from register.forms import LoginForm
from django.contrib.auth import get_user_model
import json
from django.http import JsonResponse
from django.core import serializers

def AboutUs(request):
    form = KritikSaranForm()
    uname: str = request.user.username
    total = {
        "form" : form, 
        "greeting": "Hello, " + (uname if len(uname) <= 10 else (uname[0:5] + "...")),
        "login_form": LoginForm
    }
    return render(request, 'aboutUs/aboutUs.html', total)
    
def showText(request):
    form = KritikSaranForm()
    if request.is_ajax and request.method == 'POST':
        KritikSaran.objects.create(kritik_saran=request.POST['kritik_saran'])
        return HttpResponseRedirect('/aboutUs')
    daftar = KritikSaran.objects.all()
    jsonData = serializers.serialize('json', daftar)
    data = json.loads(jsonData)
    return JsonResponse(data, safe=False)

def tampilkan_kritik(request):
    uname: str = request.user.username
    total = {
        'data' : KritikSaran.objects.all(),
        "greeting": "Hello, " + (uname if len(uname) <= 10 else (uname[0:5] + "...")),
        "login_form": LoginForm
    }
    return render(request, "aboutUs/showText.html", total)

# def tampilkan_kritik(request):
#     if request.user.is_authenticated :
#         if request.is_ajax and request.method == 'POST':
#             KritikSaran.objects.create(kritik_saran=request.POST['kritik_saran'])
#     total = KritikSaran.objects.all()
#     data = serializers.serialize('json', total)
#     dataJson = json.loads(data)
#     return JsonResponse(dataJson, safe=False)