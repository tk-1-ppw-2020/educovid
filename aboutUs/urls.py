from django.urls import path
from django.contrib import admin
from . import views

app_name = 'aboutUs'

urlpatterns = [
    path('', views.AboutUs, name='aboutUs'),
    path('showText/', views.showText, name='showText'),
    path('aboutUs/tampilkan-kritik/', views.tampilkan_kritik, name='tampilkanKritik'),

]
