from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .models import Register
from .views import logon, signup, logoff
from .forms import LoginForm, SignUpForm
from .apps import RegisterConfig

class UserTest(TestCase):
    def setUp(self):
        # self.user_data = {
        #     'user': 'educovid_admin',
        #     'province': 'Jawa Barat'
        # }

        self.user = User.objects.create_user('educovid_admin', 'Jawa Barat')

    def test_create_user(self):
        self.assertEqual(User.objects.all().count(), 1)

    def test_str_username(self):
        self.assertEqual(str(self.user.username), 'educovid_admin')

    def test_signup_url_exists(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code, 200)

    def test_signup_html_template_exists(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register/signup.html')

    def test_signup_view_func(self):
        found = resolve('/register/')
        self.assertEquals(found.func, signup)

    def test_get_signup_func(self):
        response = Client().get(reverse('register:signup'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'register/signup.html')

    def test_post_signup_func(self):
        response = Client().post(reverse('register:signup'), {
            'first_name': 'edu',
            'last_name': 'adm',
            'username': 'edu_adm',
            'email': 'edu_adm@gmail.com',
            'password_first': '12345',
            'password_again': '12345',
            'province': 'Jawa Barat',
            }, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_signup_form_valid(self):
        form_opini = SignUpForm(data={
            'first_name': 'edu',
            'last_name': 'adm',
            'username': 'edu_adm',
            'email': 'edu_adm@gmail.com',
            'password_first': '12345',
            'password_again': '12345',
            'province': 'Jawa Barat',
        })
        self.assertTrue(form_opini.is_valid())

    def test_signup_form_invalid(self):
        form_opini = SignUpForm(data={})
        self.assertFalse(form_opini.is_valid())

    def test_post_signup_form(self):
        response = Client().post('/', data={
            'first_name': 'edu',
            'last_name': 'adm',
            'username': 'edu_adm',
            'email': 'edu_adm@gmail.com',
            'password_first': '12345',
            'password_again': '12345',
            'province': 'Jawa Barat',
        })
        self.assertEqual(response.status_code, 200)

    # def test_create_signup_form(self):
    #     response = Client().post(reverse('register:signup'), data={
    #         'first_name': 'edu',
    #         'last_name': 'adm',
    #         'username': 'edu_adm',
    #         'email': 'edu_adm@gmail.com',
    #         'password_first': '12345',
    #         'password_again': '12345',
    #         'province': 'Jawa Barat',
    #     })
    #     self.assertEqual(response.status_code, 200)

    def test_register_app(self):
        self.assertEquals(RegisterConfig.name, 'register')
        self.assertEquals(apps.get_app_config('register').name, 'register')
