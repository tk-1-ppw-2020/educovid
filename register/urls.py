from django.urls import path, include, re_path
from . import views

app_name = 'register'

urlpatterns = [
    path('', views.signup, name='signup'),
    path('logon/', views.logon, name='logon'),
    path('logoff/', views.logoff, name='logoff'),
]
