from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from .models import Register
from .forms import SignUpForm, LoginForm

User = get_user_model()

def logon(request):
    form = LoginForm(request.POST or None)
    context = {
        'form': form
    }
    print(request.user.is_authenticated)
    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            print("ERROR")
    else:
        print(form.errors)
    return redirect('/')
    

def signup(request):
    form = SignUpForm(request.POST or None)
    uname: str = request.user.username
    context = {
        'form': form,
        "login_form": LoginForm
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password_first")
        province = form.cleaned_data.get("province")
        user = User.objects.create_user(username, email, password)
        Register.objects.create(user=user, province=province)
        #auto-login
        loginUser = authenticate(username=username, password=password)
        login(request, loginUser)
        return HttpResponseRedirect('/')   
    return render(request, 'register/signup.html', context=context)

def logoff(request):
    logout(request)
    return redirect('/')
