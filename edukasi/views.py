from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse, HttpResponse
from .forms import OpinionForm
from .models import Opinion
from register.forms import LoginForm
from django.forms.models import model_to_dict
import json

response_data = {}
def edukasi(request):
    form_opini = OpinionForm()
    uname = ''

    # request.is_ajax() and 
    if request.method == 'POST':
        form_opini = OpinionForm(request.POST)

        # if request.user.is_authenticated:
        uname: str = request.user.username

        if form_opini.is_valid():
            data = form_opini.cleaned_data

            opini_form = data['opini_form']
            opini_form = opini_form if len(opini_form) <= 50 else opini_form[:50]
            
            opini = Opinion.objects.create(
                opini = opini_form
            )

            id = opini.id

            response_data = {
                'uname': uname,
                'opini': opini_form,
                'id' : id
            }

            return JsonResponse(response_data, safe=False)

    uname: str = request.user.username
    context = {
        'form': form_opini.as_table, 
        'opinions': Opinion.objects.filter().order_by('-date')[:10],
        # 'opinions': Opinion.objects.all().order_by('-date'),
        'greeting': 'Hello, ' + (uname if len(uname) <= 10 else (uname[0:5] + '...')),
        'login_form': LoginForm
    }

    return render(request, 'edukasi/edukasi.html', context)

    # form_opini = OpinionForm()
    # opini_model = Opinion.objects.all()
    # uname = ""
    # response_data = {}

    # if request.POST.get('action') == 'POST':
    #     if request.user.is_authenticated:
    #         uname: str = request.user.username
    #         opini = request.POST.get('opini')

    #         response_data['uname'] = uname
    #         response_data['opini'] = opini

    #         Opinion.objects.create(
    #             opini = opini
    #         )
    #         return JsonResponse(response_data)

    
    # context = { 
    # 'opinions':Opinion.objects.all().order_by('-date'),
    # "greeting": "Hello, " + (uname if len(uname) <= 10 else (uname[0:5] + "...")),
    # "opini_model": opini_model,
    # "login_form": LoginForm
    # }
    # return render(request, 'edukasi/edukasi.html', context)