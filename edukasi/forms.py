from django import forms

class OpinionForm(forms.Form):
    opini_form = forms.CharField(
        label="",
        max_length=150,
        widget=forms.TextInput(attrs={'style': 'width: 40%; height: 15vh; border-radius: 25px;', 'id': 'opini'}),
    )