from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .models import Opinion
from .views import edukasi
from .forms import OpinionForm
from .apps import EdukasiConfig
import json

class ModelsTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user(username='jaja', password='1234')
        self.new_user.save()
        self.opini = Opinion.objects.create(opini="Cara penyebaran COVID-19 sangat berbahaya")

    def test_opini_created(self):
        count_opini = Opinion.objects.all().count()
        self.assertEquals(count_opini, 1)

    def test_str_opini(self):
        self.assertEquals(str(self.opini), "Cara penyebaran COVID-19 sangat berbahaya")

class URLsTest(TestCase):
    def test_edukasi_url_exists(self):
        response = Client().get('/edukasi/')
        self.assertEquals(response.status_code, 200)

class HTMLTest(TestCase):
    def test_edukasi_html_template_exists(self):
        response = Client().get('/edukasi/')
        self.assertTemplateUsed(response, 'edukasi/edukasi.html')

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.new_user = User.objects.create_user(username='jaja', password='1234')
        self.new_user.save()
        self.Opinion = Opinion.objects.create(opini='menakutkan')
        self.response = self.client.login(
            username = "jaja",
            password = "1234"
        )
        self.Opinion = Opinion.objects.create(opini='menakutkan')

        # self.user = User.objects.create_user(first_name='jaja', last_name='najwa', username='jaja', email='jaja@gmail.com', password_first='1234', password_again='1234', province='Jawa Barat')
        # self.user.save()
	    # self.user_login = self.client.login(username='jaja', password='1234')
        self.model = reverse("edukasi:edukasi")

    def test_edukasi_view_func(self):
        found = resolve('/edukasi/')
        self.assertEquals(found.func, edukasi)

    def test_get_edukasi_func(self):
        response = Client().get(self.model)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'edukasi/edukasi.html')

    def test_post_edukasi_func(self):
        response = Client().post(self.model, {'opini_form': 'covid menakutkan'}, follow = True)
        self.assertEqual(response.status_code, 200)

    # def test_opini_valid_then_redirect(self):
    #     response = Client().post('/edukasi/', data={'opini_form': 'covid menakutkan'}, follow=True)
    #     self.assertEquals(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'edukasi/edukasi.html')

    # def test_opini_invalid_then_redirect(self):
    #     response = Client().post('/edukasi/', data={'opini_form': ' '}, follow=True)
    #     self.assertEquals(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'edukasi/edukasi.html')

    def test_json(self):
        opini_dict = {
            'opini': 'covid menakutkan'
        }
        response = Client().post('/edukasi/', json.dumps(opini_dict), content_type="application/json")
        self.assertEqual(response.status_code, 200)

class FormsTest(TestCase):
    def setUp(self):
        self.opini = Opinion.objects.create(opini="Cara penyebaran COVID-19 sangat berbahaya")
        self.opini.save()
        self.edukasi = reverse('edukasi:edukasi')

    def test_form_valid(self):
        form_opini = OpinionForm(data={'opini_form': self.opini})
        self.assertTrue(form_opini.is_valid())

    def test_form_invalid(self):
        form_opini = OpinionForm(data={})
        self.assertFalse(form_opini.is_valid())

    def test_post_form(self):
        response = Client().post('/edukasi/', data={'opini': 'Cara penyebaran COVID-19 sangat berbahaya'})
        self.assertEqual(response.status_code, 200)

    def test_create_opini_form(self):
        response = Client().post(self.edukasi, data={
            'opini': 'Cara penyebaran COVID-19 sangat berbahaya'
        })
        self.assertEqual(response.status_code, 200)

class AppTest(TestCase):
    def test_edukasi_app(self):
        self.assertEquals(EdukasiConfig.name, 'edukasi')
        self.assertEquals(apps.get_app_config('edukasi').name, 'edukasi')