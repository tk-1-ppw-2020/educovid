$( function() {
    $('#formOpini').on('submit', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $.ajax({
            type: 'POST',
            url: '/edukasi/',
            data: {
                opini_form: $("#opini").val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                action: 'post'
            },
            success: function(response) {
                let table = $('tbody');

                // $('#opiniId').html(response.id);
                // $('#opiniData').html(response.opini);
                // $('#opiniUname').html(response.uname);
                // console.log(response);
                document.getElementById("formOpini").reset();

                // var uname = $('<td>').append(response.uname);
                var opini = $('<td>').append(response.opini);

                var new_row = $('<tr>').append(opini);

                $(table).prepend(new_row);
            },
            error : function(error) {
            }      
        })
    })

    $( "#accordionCaraPenularan" ).accordion({
        active: false,
        collapsible: true,
        
    })

    $( "#accordionGejala" ).accordion({
        active: false,
        collapsible: true,        
    });
});

i = 0;
$(document).ready(function(){
    $("form").click(function() {
        $("span").text(i = 0);
    })
    $("form").keydown(function(){
        if (event.keyCode == 8 || event.keyCode == 46) {
            i -= 1;

            if (i < 0) {
                $("span").text(i = 0);
            } else {
                $("span").text(i);
            }
        } else {
            $("span").text(i += 1);
        }
    });
});

$(document).ready(function(){
    $("img").fadeOut();
    $(".btn1").click(function(){
        $("img").fadeOut();
    });
    $(".btn2").click(function(){
        $("img").fadeIn();
    });
});